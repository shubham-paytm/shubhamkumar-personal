import com.google.gson.Gson
import model.TapNPay
import java.io.BufferedReader
import java.io.File

fun getJsonAsString(): String {
    val path = System.getProperty("user.dir")
    val bufferedReader: BufferedReader = File("$path/rollout.json").bufferedReader()
    val rolloutString = bufferedReader.use { it.readText() }
    //println("rolloutString:\t $rolloutString")
    return rolloutString
}

fun main() {
    print("main run")

    val gson = Gson()
    val tapNPayRollout: TapNPay = gson.fromJson(getJsonAsString(), TapNPay::class.java)

    tapNPayRollout.tapNPayRolloutConfig?.let {
        val rollout = TapAndPayRolloutImpl(it)

//        println(rollout.isUserInRestrictedRollout("123"))
//        println(rollout.isUserInRestrictedRollout("769357932875091"))
//        println(rollout.isUserInRestrictedRollout("gjbfkebglng"))
//        println(rollout.isUserInRestrictedRollout("12345"))
//        println(rollout.isUserInRestrictedRollout("121"))

        println("\n\nisEnrolledForProductRollout-------\n\n")

//        println(rollout.isEnrolledForProductRollout("1321"))
//        println(rollout.isEnrolledForProductRollout("123"))
//        println(rollout.isEnrolledForProductRollout("1234"))
//        println(rollout.isEnrolledForProductRollout("12345"))
//        println("of\t ${rollout.isEnrolledForProductRollout("of")}")
//        println("lasa\t ${rollout.isEnrolledForProductRollout("lasa")}")
//        println("custIds\t ${rollout.isEnrolledForProductRollout("custIds")}")


        println("\n\nisEnrolledForBankRollout-------\n")

//        println(rollout.isEnrolledForBankRollout("1321", "icici", "credit", "visa"))
//        println(rollout.isEnrolledForBankRollout("custIds", "icici", "debit", "visa"))
//        println(rollout.isEnrolledForBankRollout("custIds", "hdfc", "debit", "master"))
//        println(rollout.isEnrolledForBankRollout("1234", "hdfc", "debit", "master"))
//        println(rollout.isEnrolledForBankRollout("12345", "hdfc", "debit", "master"))
        println(rollout.isEnrolledForBankRollout("1321", "hdfc", "debit", "master"))
        println(rollout.isEnrolledForBankRollout("1321", "icici", "debit", "master"))
        println(rollout.isEnrolledForBankRollout("1321", "ujayan", "debit", "master"))

    }

    //println(tapNPayRollout)
}