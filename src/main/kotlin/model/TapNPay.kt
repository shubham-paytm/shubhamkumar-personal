package model


import com.google.gson.annotations.SerializedName

data class TapNPay(
    @SerializedName("tap_n_pay_rollout_config")
    val tapNPayRolloutConfig: TapNPayRolloutConfig? = null
)