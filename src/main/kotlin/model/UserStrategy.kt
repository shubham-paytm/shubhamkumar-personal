package model


import com.google.gson.annotations.SerializedName

data class UserStrategy(
    @SerializedName("allow_all_users")
    val allowAllUsers: Boolean = false, // false
    @SerializedName("user_id_ends_with")
    val userIdEndsWith: List<String>? = null,
    @SerializedName("users_id")
    val usersId: List<String>? = null
)