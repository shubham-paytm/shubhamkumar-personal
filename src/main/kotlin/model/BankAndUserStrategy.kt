package model


import com.google.gson.annotations.SerializedName

data class BankAndUserStrategy(
    @SerializedName("code")
    val code: String? = null, // icici
    @SerializedName("limitedUsers")
    val limitedUsers: List<String>? = null,
    @SerializedName("scheme")
    val scheme: String? = null, // $visa$maestro$master$rupay$
    @SerializedName("type")
    val type: String? = null // $debit$credit$
)