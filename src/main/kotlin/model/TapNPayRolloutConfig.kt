package model


import com.google.gson.annotations.SerializedName

data class TapNPayRolloutConfig(
    @SerializedName("name")
    val name: String? = null, // tokenization
    @SerializedName("revision")
    val revision: String? = null, // optional
    @SerializedName("strategy")
    val strategy: Strategy? = null,
    @SerializedName("type")
    val type: String? = null // feature
)