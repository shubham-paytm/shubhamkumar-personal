package model


import com.google.gson.annotations.SerializedName

data class Strategy(
    @SerializedName("bank_and_user_strategy")
    val bankAndUserStrategy: List<BankAndUserStrategy>? = null,
    @SerializedName("restricted_user_ids")
    val restrictedUserIds: List<String>? = null,
    @SerializedName("user_strategy")
    val userStrategy: UserStrategy? = null
)