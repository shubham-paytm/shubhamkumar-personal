import model.TapNPayRolloutConfig

class TapAndPayRolloutImpl(private val rollout: TapNPayRolloutConfig) : TapAndPayRollout {

    private var mEnrolledForProduct: Boolean? = null

    /**
     * It includes if user can test out TapNPay functionality
     */
    override fun isEnrolledForProductRollout(userId: String): Boolean {
//        if (mEnrolledForProduct != null) {
//            return mEnrolledForProduct == true
//        }
        mEnrolledForProduct = !isUserInRestrictedRollout(userId) &&
                (isAllowedAllUser() || isUserFillsEndsWithCriteria(userId)
                        || isUserInProdEnrolledList(userId) || isUserInBankLimitedList(userId))

        return mEnrolledForProduct == true
    }

    override fun isEnrolledForBankRollout(userId: String, bankCode: String, type: String, scheme: String): Boolean {
        if (!isEnrolledForProductRollout(userId)) {
            return false
        }
        rollout.strategy?.bankAndUserStrategy?.forEach {
            val bankCodeMatch = it.code?.contains(bankCode) == true
            val cardTypeMatch = it.type?.contains(type) == true
            val cardSchemeMatch = it.scheme?.contains(scheme) == true

            if (bankCodeMatch && cardTypeMatch && cardSchemeMatch) {
                return it.limitedUsers.isNullOrEmpty() || it.limitedUsers.contains(userId)
            }
        }
        return false
    }

    override fun isUserInRestrictedRollout(userId: String): Boolean {
        val restrictedUsers = rollout.strategy?.restrictedUserIds?.toSet() ?: setOf()
        println("RestrictedRollout ${restrictedUsers.contains(userId)}")
        return restrictedUsers.contains(userId)
    }

    private fun isAllowedAllUser(): Boolean {
        println("AllowedAllUser ${rollout.strategy?.userStrategy?.allowAllUsers == true}")
        return rollout.strategy?.userStrategy?.allowAllUsers == true
    }

    private fun isUserFillsEndsWithCriteria(userId: String): Boolean {
        println("FillsEndsWithCriteria ${(rollout.strategy?.userStrategy?.userIdEndsWith?.any { userId.endsWith(it, ignoreCase = true) }) == true}")
        return (rollout.strategy?.userStrategy?.userIdEndsWith?.any { userId.endsWith(it, ignoreCase = true) }) == true
    }

    private fun isUserInProdEnrolledList(userId: String): Boolean {
        val productEnrolledUserIds = rollout.strategy?.userStrategy?.usersId?.toSet() ?: setOf()
        println("InProdEnrolledList ${productEnrolledUserIds.contains(userId)}")

        return productEnrolledUserIds.contains(userId)
    }

    private fun isUserInBankLimitedList(userId: String): Boolean {
        return rollout.strategy?.bankAndUserStrategy?.any { it.limitedUsers?.contains(userId) == true } == true
    }
}