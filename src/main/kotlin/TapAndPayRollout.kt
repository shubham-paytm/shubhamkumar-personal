
interface TapAndPayRollout {
    fun isEnrolledForProductRollout(userId: String):Boolean
    fun isEnrolledForBankRollout(userId: String, bankCode: String, type: String, scheme: String):Boolean
    fun isUserInRestrictedRollout(userId: String) :Boolean
}