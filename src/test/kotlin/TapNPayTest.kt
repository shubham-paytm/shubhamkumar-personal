import com.google.gson.Gson
import model.TapNPay
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.io.BufferedReader
import java.io.File
import kotlin.random.Random

class TapNPayTest {

    private lateinit var tapNPayRollout: TapAndPayRollout

    @BeforeEach
    fun configureSystemUnderTest() {
        val gson = Gson()
        val tapNPay = gson.fromJson(getJsonAsString(), TapNPay::class.java)

        tapNPay.tapNPayRolloutConfig?.let {
            tapNPayRollout = TapAndPayRolloutImpl(it)
        }
    }

    @Test
    @DisplayName("TapNPay is initialized properly")
    fun shouldBeInitialized() {
        assertThat(tapNPayRollout).isNotNull
    }

    @Test
    @DisplayName("Should Have Allowed AllUsers")
    fun shouldAllowAllUser() {
        val randomUser = (java.util.Random().nextFloat()*10000L).toString()
        assertThat(tapNPayRollout.isEnrolledForProductRollout(randomUser)).isTrue()
    }

    @Test
    @DisplayName("Should allow BankOnly Users")
    fun shouldAllowAllBankUser() {
        val randomUser = (java.util.Random().nextFloat()*10000L).toString()
        assertThat(tapNPayRollout.isEnrolledForProductRollout(randomUser)).isTrue()
    }





    private fun getJsonAsString(): String {
        val path = System.getProperty("user.dir")
        val bufferedReader: BufferedReader = File("$path/rollout.json").bufferedReader()
        val rolloutString = bufferedReader.use { it.readText() }
        //println("rolloutString:\t $rolloutString")
        return rolloutString
    }
}